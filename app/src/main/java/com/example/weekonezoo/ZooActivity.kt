package com.example.weekonezoo

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.navArgs

class ZooActivity : AppCompatActivity() {
    val args by navArgs<ZooActivityArgs>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_zoo)

        Toast.makeText(this, "Welcome to ${args.name}", Toast.LENGTH_SHORT).show()

    }


}
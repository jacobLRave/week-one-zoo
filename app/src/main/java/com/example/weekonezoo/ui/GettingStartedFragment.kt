package com.example.weekonezoo.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.weekonezoo.databinding.FragmentGettingStartedBinding

class GettingStartedFragment : Fragment() {

    private var _binding: FragmentGettingStartedBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentGettingStartedBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val next = GettingStartedFragmentDirections.actionGettingStartedFragmentToFinishFragment()
        val back = GettingStartedFragmentDirections.actionGettingStartedFragmentToHelloFragment()

        binding.btnGettingStartedNext.setOnClickListener {
            findNavController().navigate(next)
            println("clicked")
        }
        binding.btnGettingStartedBack.setOnClickListener {
            findNavController().navigate(back)
            println("clicked")
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
package com.example.weekonezoo.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.weekonezoo.databinding.FragmentFinishBinding

class FinishFragment : Fragment() {

    private var _binding: FragmentFinishBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentFinishBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val back = FinishFragmentDirections.actionFinishFragmentToGettingStartedFragment()
        val next = FinishFragmentDirections.actionFinishFragmentToZooActivity("Manila Zoo")

        binding.btnFinishBack.setOnClickListener {
            findNavController().navigate(back)
        }

        binding.btnFinishNext.setOnClickListener {
            findNavController().navigate(next)

        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
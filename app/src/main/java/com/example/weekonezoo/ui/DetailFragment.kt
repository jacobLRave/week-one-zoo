package com.example.weekonezoo.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.weekonezoo.databinding.FragmentDetailBinding

class DetailFragment : Fragment() {

    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<DetailFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDetailBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val back = DetailFragmentDirections.actionDetailFragmentToHomeFragment()
        binding.tvTitle.text = args.name
        binding.tvDetails.text = args.details
        binding.btnDetailsBack.setOnClickListener {
            findNavController().navigate(back)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
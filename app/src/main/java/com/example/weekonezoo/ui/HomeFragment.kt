package com.example.weekonezoo.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.weekonezoo.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val hippoDirection =
            HomeFragmentDirections.actionHomeFragmentToDetailFragment("Hippo", "Killer animal")
        val rabbitDirection = HomeFragmentDirections.actionHomeFragmentToDetailFragment(
            "Rabbit",
            "Super killer animal"
        )
        val dragonDirection = HomeFragmentDirections.actionHomeFragmentToDetailFragment(
            "Dragon",
            "Flying mythical killer animal"
        )
        val pterodactylDirection = HomeFragmentDirections.actionHomeFragmentToDetailFragment(
            "Pterodactyl",
            "Dino killer animal"
        )

        binding.btnHomeHippo.setOnClickListener {
            findNavController().navigate(hippoDirection)
        }
        binding.btnHomeRabbit.setOnClickListener {
            findNavController().navigate(rabbitDirection)
        }
        binding.btnHomeDragon.setOnClickListener {
            findNavController().navigate(dragonDirection)
        }
        binding.btnHomePterodactyl.setOnClickListener {
            findNavController().navigate(pterodactylDirection)
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}